# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the openstore-web package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: openstore-web 1.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-21 22:39-0400\n"
"PO-Revision-Date: 2019-01-02 13:42+0000\n"
"Last-Translator: Charles Stevens <yugioh2017@yandex.com>\n"
"Language-Team: Arabic <https://translate.ubports.com/projects/openstore/open-"
"storeio/ar/>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Weblate 3.1.1\n"

#: src/views/Package.vue:118
msgid ""
"*This app contains NSFW content, to view the description and\n"
"screenshots, click the above button."
msgstr ""
"*هذا التطبيق يتضمن محتوى للبالغين, لمشاهدة التفاصيل ولقطات من التطبيق, انقر "
"على الزر في الأعلى."

#: src/views/ManagePackage.vue:577
msgid "%{name} was successfully deleted"
msgstr "تم حذف %{name} بنجاح"

#: src/views/ManagePackage.vue:157
msgid "A comma separated list of keywords"
msgstr "قائمة كلمات المفتاحية  مفصولة بفواصل"

#: src/App.vue:34 src/views/About.vue:3 src/views/About.vue:183
#: src/views/About.vue:187
msgid "About"
msgstr "عن"

#: src/App.vue:78
msgid "About Us"
msgstr "عن الموقع"

#: src/views/Package.vue:266
msgid "Accounts"
msgstr "الحسابات"

#: src/views/Browse.vue:16
msgid "All Categories"
msgstr "جميع التصنيفات"

#: src/views/Browse.vue:31
msgid "All Types"
msgstr "كل الانواع"

#: src/views/Login.vue:26
msgid "An account is not needed to download or install apps."
msgstr "يتوجب عليك التسجيل لتتمكن من تحميل او تنصيب التطبيقات."

#: src/views/ManagePackage.vue:444 src/views/ManagePackage.vue:462
#: src/views/ManageRevisions.vue:136
msgid "An error occured loading your app data"
msgstr "حدث خطأ اثناء تحميل معلومات التطبيق"

#: src/views/ManageRevisions.vue:183 src/views/Submit.vue:212
msgid "An unknown error has occured"
msgstr "حدث خطأ غير معروف"

#: src/views/About.vue:160
msgid ""
"And many amazing developers, testers, and users from the OpenStore community!"
msgstr "والعديد من المطورون الرائعون, المجربون, ومستخدمون من مجتمع OpenStore!"

#: src/views/About.vue:122
msgid ""
"Any personal data you decide to give us (e.g. your email address when "
"registering for an account to upload your apps)\ngoes no further than us,\n"
"and will not be used for anything other than allowing you to maintain your "
"apps and, in rare cases,\n"
"contact you in the event there is an issue with your app.\n"
"Additionally, this site uses cookies to keep you logged in to your account."
msgstr ""
"أي بيانات شخصية تقرر منحنا اياها (على سبيل المثال ، عنوان بريدك الإلكتروني "
"عند التسجيل للحصول على حساب لتحميل تطبيقاتك) لا تذهب لطرف آخر،\n"
"ولن يتم استخدامه لأي شيء بخلاف السماح لك بالحفاظ على تطبيقاتك ، وفي حالات "
"نادرة ، الاتصال بك في حالة وجود مشكلة في تطبيقك.\n"
"بالإضافة إلى ذلك ، هذا الموقع يستخدم ملفات تعريف الارتباط(الكوكيز)للحفاظ على "
"تسجيل الدخول إلى حسابك فقط."

#: src/views/Manage.vue:14
msgid "API Key"
msgstr "مفتاح API"

#: src/components/Types.vue:3 src/views/Manage.vue:46 src/views/Stats.vue:108
msgid "App"
msgstr "تطبيق"

#: src/views/Submit.vue:95
msgid "App containing or promoting any of the above are strictly prohibited."
msgstr "يُحظر تمامًا تطبيق يحتوي على أي من العناصر الواردة أعلاه أو يروج له."

#: src/views/Submit.vue:133
msgid "App Name"
msgstr "اسم التطبيق"

#: src/App.vue:88
msgid "App Stats"
msgstr "إحصائيات التطبيق"

#: src/views/Submit.vue:150
#, fuzzy
msgid "App Title"
msgstr "العنوان"

#: src/views/Stats.vue:19
msgid "App Type"
msgstr "نوع التطبيق"

#: src/views/Stats.vue:15
msgid "App Types"
msgstr "انواع التطبيقات"

#: src/views/Browse.vue:32
msgid "Apps"
msgstr "التطبيقات"

#: src/views/Submit.vue:101
msgid ""
"Apps containing gambling involving real world money transactions are not "
"allowed.\n"
"This includes, but is not limited to, online casinos, betting, and lotteries."
msgstr ""
"لا يُسمح بالتطبيقات التي تحتوي على المقامرة التي تتضمن معاملات مالية حقيقية.\n"
"وهذا يشمل ، على سبيل المثال لا الحصر ، الكازينوهات على الإنترنت ، "
"والمراهنات ، واليانصيب."

#: src/views/Submit.vue:82
msgid ""
"Apps containing or promoting child abuse or child sexual abuse are strictly "
"prohibited."
msgstr ""
"يُحظر تمامًا استخدام التطبيقات التي تحتوي على إساءة معاملة الأطفال أو الإساءة "
"الجنسية للأطفال أو تروّج لها."

#: src/views/Submit.vue:88
msgid ""
"Apps containing or promoting gratuitous violence are not allowed.\n"
"This includes, but is not limited to, violence, terrorism, bomb/weapon "
"making, self harm, and grotesque imagery."
msgstr ""
"لا يُسمح بالتطبيقات التي تحتوي على العنف غير المبرر أو تروج له.\n"
"وهذا يشمل ، على سبيل المثال لا الحصر ، العنف والإرهاب وصنع القنابل / صنع "
"الأسلحة وأذى النفس والصور الغريبة."

#: src/views/Submit.vue:75
msgid ""
"Apps containing or promoting sexually explicit content are not allowed.\n"
"This includes, but is not limited to, pornography and services promoting "
"sexually explicit content."
msgstr ""
"لا يُسمح بالتطبيقات التي تحتوي على محتوى جنسي صريح أو تروج له.\n"
"وهذا يشمل ، على سبيل المثال لا الحصر ، المواد الإباحية والخدمات التي تروج "
"للمحتوى الجنسي الصريح."

#: src/views/Submit.vue:7
msgid ""
"Apps not requiring manual review can be submitted by logging in with the "
"\"Log In\" link above."
msgstr ""
"يمكن رفع التطبيقات التي لا تتطلب مراجعة يدوية عن طريق تسجيل الدخول باستخدام "
"رابط \"تسجيل الدخول\" أعلاه."

#: src/views/Submit.vue:108
msgid "Apps promoting illegal activities (based on USA law) are not allowed."
msgstr ""
"لا يُسمح بالتطبيقات التي تروج لأنشطة غير قانونية (بناءً على قانون الولايات "
"المتحدة الأمريكية)."

#: src/views/Submit.vue:18
msgid ""
"Apps requiring manual review must be submitted by contacting an admin in our "
"telegram group."
msgstr ""
"يجب تقديم التطبيقات التي تتطلب مراجعة يدوية من خلال الاتصال بمشرف في مجموعة "
"التلغرام الخاصة بنا."

#: src/views/Submit.vue:114
msgid ""
"Apps that are found to be stealing the users data, trying to escalate their "
"privileges without user consent,\n"
"or executing malicious process are strictly prohibited."
msgstr ""
"التطبيقات التي يتم اكتشاف انها تقوم بسرقة بيانات المستخدمين ، ومحاولة زيادة "
"صلاحياتها دون موافقة المستخدم ،\n"
"أو تنفيذ عملية خبيثة في النظام ممنوعة منعاً باتاً."

#: src/views/About.vue:129
msgid ""
"Apps uploaded to the OpenStore are subject to our submission rules and "
"content policy.\n"
"You can review the rules and policies before uploading your app."
msgstr ""
"تخضع التطبيقات التي يتم تحميلها إلى OpenStore لقواعد التقديم وسياسة المحتوى "
"الخاصة بنا.\n"
"يمكنك مراجعة القواعد والسياسات قبل تحميل تطبيقك."

#: src/views/ManagePackage.vue:258 src/views/Package.vue:193
msgid "Architecture"
msgstr "البنية"

#: src/views/Package.vue:267
msgid "Audio"
msgstr "صوت"

#: src/views/Manage.vue:47 src/views/ManagePackage.vue:254
#: src/views/Package.vue:155
msgid "Author"
msgstr "المقدّم"

#: src/views/Package.vue:7
msgid "Back"
msgstr "العودة"

#: src/views/Badge.vue:28 src/views/Badge.vue:32 src/views/ManagePackage.vue:70
msgid "Badge"
msgstr ""

#: src/views/About.vue:18
msgid "Badges"
msgstr ""

#: src/views/Package.vue:268
msgid "Bluetooth"
msgstr "بلوتوث"

#: src/App.vue:31
msgid "Browse"
msgstr "تصفح"

#: src/views/Browse.vue:162
msgid "Browse Apps"
msgstr "تصفح التطبيقات"

#: src/views/About.vue:74
msgid "Bug Tracker"
msgstr "مكتشف المشاكل البرمجية"

#: src/views/Package.vue:269
msgid "Calendar"
msgstr "التقويم"

#: src/views/Package.vue:270
msgid "Camera"
msgstr "كاميرا"

#: src/views/ManagePackage.vue:358 src/views/ManageRevisions.vue:66
#: src/views/Submit.vue:167
msgid "Cancel"
msgstr "إلغاء"

#: src/views/Browse.vue:14 src/views/ManagePackage.vue:140
#: src/views/Package.vue:177
msgid "Category"
msgstr "تصنيف"

#: src/views/ManagePackage.vue:101 src/views/ManageRevisions.vue:50
#: src/views/Package.vue:128
msgid "Changelog"
msgstr "سجل التغيرات"

#: src/views/ManagePackage.vue:295 src/views/ManageRevisions.vue:13
msgid "Channel"
msgstr "القناة"

#: src/views/ManagePackage.vue:274
msgid "Channels"
msgstr "القنوات"

#: src/App.vue:83
msgid "Chat with us on Telegram"
msgstr "تحدث معنا على تلغرام"

#: src/views/Submit.vue:81
msgid "Child Endangerment"
msgstr "الإساءة للإطفال"

#: src/views/ManagePackage.vue:142
msgid "Choose a category"
msgstr "اختر تصنيف"

#: src/views/ManagePackage.vue:173
msgid "Choose a license"
msgstr "اختر ترخيص"

#: src/views/ManagePackage.vue:231
msgid "Choose a maintainer"
msgstr "اختيار مشرف"

#: src/views/Package.vue:271
msgid "Connectivity"
msgstr "التوصيل"

#: src/views/About.vue:59
msgid "Contact"
msgstr "اتصل"

#: src/views/Package.vue:272
msgid "Contacts"
msgstr "جهات الاتصال"

#: src/views/Package.vue:274
msgid "Content Exchange"
msgstr "تبادل المحتوى"

#: src/views/Package.vue:273
msgid "Content Exchange Source"
msgstr "مصدر تبادل المحتويات"

#: src/views/About.vue:138
msgid "Contributors"
msgstr "المساهمون"

#: src/components/CopyLine.vue:9
msgid "Copy to clipboard"
msgstr "نسخ"

#: src/views/Stats.vue:20
msgid "Count"
msgstr "العدد"

#: src/views/ManageRevisions.vue:57
msgid "Create"
msgstr "إنشاء"

#: src/views/Package.vue:275
msgid "Debug"
msgstr "معالجة"

#: src/views/ManagePackage.vue:353
msgid "Delete"
msgstr "حذف"

#: src/views/ManagePackage.vue:90
msgid "Description"
msgstr "التفاصيل"

#: src/views/ManagePackage.vue:55
msgid "Discovery"
msgstr "اكتشاف"

#: src/views/Package.vue:91
msgid "Donate"
msgstr "تبرع"

#: src/views/ManagePackage.vue:216
msgid "Donate URL"
msgstr "رابط التبرع"

#: src/views/Package.vue:262
#, fuzzy
msgid "Download"
msgstr "التنزيلات"

#: src/views/ManagePackage.vue:288
msgid "Download Stats"
msgstr "إحصائيات التحميل"

#: src/views/About.vue:43
msgid "Download the OpenStore App"
msgstr "حمّل تطبيق OpenStore"

#: src/views/Manage.vue:50 src/views/Manage.vue:70
#: src/views/ManagePackage.vue:297
msgid "Downloads"
msgstr "التنزيلات"

#: src/views/ManagePackage.vue:119
msgid "Drag and drop to sort screenshots."
msgstr "قم بالسحب والإفلات لترتيب لقطات الشاشة."

#: src/views/ManagePackage.vue:8
msgid "Edit"
msgstr "تعديل"

#: src/views/ManagePackage.vue:443 src/views/ManagePackage.vue:461
#: src/views/ManagePackage.vue:562 src/views/ManagePackage.vue:590
#: src/views/ManageRevisions.vue:135 src/views/Submit.vue:218
msgid "Error"
msgstr "خطأ"

#: src/views/Feeds.vue:34 src/views/Feeds.vue:38
msgid "Feeds"
msgstr "التغذيات"

#: src/views/ManagePackage.vue:266
msgid "File Size"
msgstr "حجم الملف"

#: src/views/ManageRevisions.vue:22
msgid "File Upload"
msgstr "رفع الملف"

#: src/views/About.vue:61
msgid ""
"For any questions you may have or help you may need just chat with us in our "
"telegram group.\n"
"You can also submit issues, bugs, and feature requests to our GitLab bug "
"tracker."
msgstr ""
"لأي أسئلة قد تكون لديك أو مساعدة تحتاجها فأنك فقط تحتاج لمخاطبتنا على مجموعة "
"تلغرام الخاصة بنا.\n"
"يمكنك أيضا التبليغ عن المشاكل ، والأخطاء البرمجية ، وطلبات ميزات جديدة إلى "
"منصة تعقب الأخطاء الخاص بنا على GitLab."

#: src/App.vue:93
msgid "Fork us on GitLab"
msgstr "شاركنا على GitLab"

#: src/views/ManagePackage.vue:262 src/views/Package.vue:189
msgid "Framework"
msgstr "منصة البناء"

#: src/views/Package.vue:293
msgid "Full System Access"
msgstr "وصول كامل للنظام"

#: src/views/Submit.vue:100
msgid "Gambling"
msgstr "المقامرة"

#: src/views/About.vue:25
msgid "Get your badge"
msgstr ""

#: src/views/Submit.vue:39
msgid ""
"Give a short explanation why you can’t publish the app without extra "
"privileges.\n"
"No need to go into details, a one liner like \"needs to run a daemon in the "
"background\" will do.\n"
"List all the special features you have, if there are more."
msgstr ""
"قدِّم شرحًا مختصرًا لماذا لا يمكنك نشر التطبيق بدون صلاحيات إضافية.\n"
"لا حاجة للخوض في التفاصيل ، سطر واحد مثل \"احتاج إلى تشغيل عملية مخفية في "
"النظام\".\n"
"قم بكتابة جميع الميزات الخاصة بالتطبيق، إن كان هناك المزيد."

#: src/views/Browse.vue:187 src/views/Manage.vue:174
msgid "Go back a page"
msgstr "العودة صفحة الى الوراء"

#: src/views/Browse.vue:188 src/views/Manage.vue:175
msgid "Go to the next page"
msgstr "الذهاب للصفحة التالية"

#: src/views/About.vue:86
msgid "GPL v3 License"
msgstr "ترخيص GPL v3"

#: src/views/Submit.vue:94
msgid "Harassment, Bullying, or Hate Speech"
msgstr "التحرش أو التسلط أو الكلام الذي يحض على الكراهية"

#: src/views/Package.vue:276
msgid "History"
msgstr "السجل"

#: src/views/Manage.vue:44
msgid "Icon"
msgstr "ايقونة"

#: src/views/ManagePackage.vue:250
msgid "ID"
msgstr "المعرف"

#: src/views/Submit.vue:44
msgid ""
"If an application could be published without manual review if it wouldn’t be "
"for that one cool feature,\npublish a stripped down version!\n"
"Not everyone will want to install apps with less confinement."
msgstr ""
"إذا كان من الممكن نشر تطبيق ما دون مراجعة يدوية إذا لم يكن لهذا الميزة "
"الرائعة ، فقم بنشر نسخة مخففة!\n"
"لن يرغب الجميع في تثبيت التطبيقات مع ميزات أقل."

#: src/views/Submit.vue:107
msgid "Illegal Activities"
msgstr "أنشطة غير قانونية"

#: src/views/Package.vue:277
msgid "In App Purchases"
msgstr "عمليات الشراء داخل التطبيق"

#: src/views/ManagePackage.vue:60 src/views/Package.vue:152
msgid "Info"
msgstr "معلومات"

#: src/views/About.vue:29 src/views/Package.vue:39
msgid "Install"
msgstr "تنصيب"

#: src/views/Package.vue:264
msgid "Install via the OpenStore app"
msgstr "تنصيب عن طريق تطبيق OpenStore"

#: src/views/Browse.vue:186 src/views/Manage.vue:173
msgid "Jump to the first page"
msgstr "الذهاب للصفحة الاولى"

#: src/views/Browse.vue:189 src/views/Manage.vue:176
msgid "Jump to the last page"
msgstr "الذهاب الى الصفحة الاخيرة"

#: src/views/Package.vue:278
msgid "Keep Display On"
msgstr "استمر في العرض"

#: src/views/Manage.vue:18
msgid "Keep your api key private!"
msgstr "حافظ على خصوصية مفتاح API الخاص بك!"

#: src/views/ManagePackage.vue:155
msgid "Keywords"
msgstr "الكلمات المفتاحية"

#: src/App.vue:60 src/components/BadgeSelect.vue:4
msgid "Language"
msgstr "اللغة"

#: src/views/Browse.vue:46
msgid "Latest Update"
msgstr "آخر تحديث"

#: src/views/About.vue:78 src/views/ManagePackage.vue:171
#: src/views/Package.vue:173
msgid "License"
msgstr "الترخيص"

#: src/views/Package.vue:279
msgid "Location"
msgstr "الموقع"

#: src/App.vue:37
msgid "Log In"
msgstr "تسجيل الدخول"

#: src/views/Login.vue:24
msgid "Log in to the OpenStore to be able to manage your apps."
msgstr "سجل دخولك الى OpenStore لتتمكن من إدارة تطبيقاتك."

#: src/App.vue:43
msgid "Log Out"
msgstr "تسجيل الخروج"

#: src/views/Login.vue:40 src/views/Login.vue:44
msgid "Login"
msgstr "تسجيل الدخول"

#: src/views/Submit.vue:125
msgid "Login to submit your app"
msgstr "سجّل دخولك لرفع تطبيقك"

#: src/views/Login.vue:7
msgid "Login via GitHub"
msgstr "تسجيل الدخول عن طريق GitHub"

#: src/views/Login.vue:12
msgid "Login via GitLab"
msgstr "تسجيل الدخول عن طريق GitLab"

#: src/views/Login.vue:18
msgid "Login via Ubuntu"
msgstr "تسجيل الدخول عن طريق حساب Ubuntu"

#: src/views/ManagePackage.vue:229
msgid "Maintainer"
msgstr "مشرف"

#: src/views/Submit.vue:113
msgid "Malware"
msgstr "البرمجيات الخبيثة"

#: src/App.vue:40 src/views/Manage.vue:152 src/views/ManagePackage.vue:381
#: src/views/ManagePackage.vue:383
msgid "Manage"
msgstr "إدارة"

#: src/views/Manage.vue:148
msgid "Manage Apps"
msgstr "إدارة التطبيقات"

#: src/views/Package.vue:280
msgid "Microphone"
msgstr "ميكروفون"

#: src/views/Package.vue:282
msgid "Music Files"
msgstr "ملفات موسيقية"

#: src/views/Manage.vue:45
msgid "Name"
msgstr "اسم"

#: src/views/About.vue:48
msgid "Navigate to the downloads folder"
msgstr "انتقل إلى مجلد التنزيلات"

#: src/views/Package.vue:283
msgid "Networking"
msgstr "الشبكة"

#: src/views/Feeds.vue:9
msgid "New Apps"
msgstr "تطبيقات جديدة"

#: src/views/ManagePackage.vue:37 src/views/ManageRevisions.vue:86
#: src/views/ManageRevisions.vue:88
msgid "New Revision"
msgstr "مراجعة جديدة"

#: src/views/ManageRevisions.vue:178
msgid "New revision for %{channel} was created!"
msgstr "قد تم إنشاء مراجعة جديدة لـ %{channel}!"

#: src/views/ManageRevisions.vue:8
msgid "New Revision for %{name}"
msgstr "مراجعة جديدة لـ %{name}"

#: src/views/Browse.vue:44
msgid "Newest"
msgstr "الأحدث"

#: src/views/ManagePackage.vue:31
msgid "No"
msgstr "لا"

#: src/views/Browse.vue:101 src/views/Manage.vue:94
msgid "No apps found."
msgstr "لم يتم العثور على تطبيقات."

#: src/views/Manage.vue:81 src/views/Manage.vue:86 src/views/Package.vue:146
msgid "None"
msgstr "لا شيء"

#: src/views/Manage.vue:75
msgid "Not published"
msgstr "غير منشور"

#: src/views/ManagePackage.vue:161
msgid "NSFW"
msgstr "محتوى بالغين"

#: src/views/Browse.vue:90
msgid "NSFW content"
msgstr "محتوى بالغين"

#: src/views/Browse.vue:45
msgid "Oldest"
msgstr "الأقدم"

#: src/views/Browse.vue:47
msgid "Oldest Update"
msgstr "أقدم تحديث"

#: src/views/Submit.vue:60
msgid ""
"One of the main goals of the OpenStore is to provide a safe app store for "
"people of all ages.\n"
"To accomplish this goal we do not allow certain types of app to be "
"published.\n"
"Failure to follow the guidelines will result in your app being removed.\n"
"Any account with serious infractions may be subject to termination.\n"
"The OpenStore team thanks you for helping us promote a better enviroment for "
"everyone."
msgstr ""
"أحد الأهداف الرئيسية لـ OpenStore هو توفير متجر تطبيقات آمن للأشخاص من جميع "
"الأعمار.\n"
"لتحقيق هذا الهدف ، لا نسمح بنشر أنواع معينة من التطبيقات.\n"
"سيؤدي عدم اتباع الإرشادات إلى إزالة تطبيقك.\n"
"أي حساب مع مخالفات خطيرة قد يتم حذفه.\n"
"فريق OpenStore يشكرك للمساعدة في ترويج بيئة أفضل وأمنة للجميع."

#: src/views/Submit.vue:48
msgid ""
"Only open source applications allowed for manual review:\n"
"As the applications might have arbitrary access to the device, every "
"manually reviewed app will get a source code review."
msgstr ""
"يُسمح فقط بتطبيقات المفتوحة المصدر للمراجعة اليدوية:\n"
"نظرًا لأن التطبيقات قد تتمتع بإمكانية الوصول العشوائي إلى الجهاز ، سيحصل كل "
"تطبيق تمت مراجعته يدويًا على مراجعة للكود المصدري الخاص به."

#: src/views/ManagePackage.vue:223
msgid ""
"Only YouTube videos are supported at this time. Make sure the url is for the "
"embedded video!"
msgstr ""
"فقط مقاطع فيديو YouTube مدعومة في الوقت الحالي. تأكد من أن الرابط هو رابط "
"التضمين الخاص بالفيديو!"

#: src/views/About.vue:47
msgid "Open the terminal app"
msgstr "افتح تطبيق الطرفية(Terminal)"

#: src/views/About.vue:145
msgid "OpenStore API/Server Contributors"
msgstr "مساهمو OpenStore في API/المخدم"

#: src/views/Submit.vue:58
msgid "OpenStore App Content Policy"
msgstr "سياسة المحتوى الخاصة بتطبيق OpenStore"

#: src/views/About.vue:155
msgid "OpenStore App Contributors"
msgstr "المساهمون في تطبيق OpenStore"

#: src/views/Badge.vue:3
msgid "OpenStore Badges"
msgstr ""

#: src/views/About.vue:67
msgid "OpenStore group on Telegram"
msgstr "مجموعة OpenStore على تلغرام"

#: src/views/About.vue:93
msgid "OpenStore source on GitLab"
msgstr "الكود المصدري لـ OpenStore على GitLab"

#: src/views/About.vue:150
msgid "OpenStore Website Contributors"
msgstr "المساهمون في الموقع الإلكتروني لـ OpenStore"

#: src/views/ManageRevisions.vue:33
msgid "OR"
msgstr "او"

#: src/views/ManagePackage.vue:245
msgid "Package Info"
msgstr "معلومات الحزمة"

#: src/views/Package.vue:162
msgid "Packager/Publisher"
msgstr "المنتج/الناشر"

#: src/views/NotFound.vue:3
msgid "Page Not Found"
msgstr "لم يتم العثور على الصفحة المطلوبة"

#: src/views/Package.vue:80 src/views/Package.vue:135
msgid "Permissions"
msgstr "الصلاحيات"

#: src/views/Package.vue:285
msgid "Picture Files"
msgstr "ملفات الصور"

#: src/views/ManagePackage.vue:43
msgid "Presentation"
msgstr "تقديم"

#: src/views/Submit.vue:69
msgid "Prohibited Apps"
msgstr "التطبيقات المحظورة"

#: src/views/About.vue:20
msgid "Promote your app on the OpenStore!"
msgstr ""

#: src/views/Badge.vue:5
msgid ""
"Promote your app on the OpenStore! Add these badges to your website,\n"
"code repository, social media, or anywhere on the internet. The badges\n"
"are available in several different languages and in both PNG and SVG\n"
"formats."
msgstr ""

#: src/views/ManagePackage.vue:14
msgid "Public Link"
msgstr "رابط عام"

#: src/views/ManagePackage.vue:21
msgid "Publish"
msgstr "نشر"

#: src/views/Manage.vue:74 src/views/Package.vue:185
msgid "Published"
msgstr "منشور"

#: src/views/ManagePackage.vue:323
msgid "Published Date"
msgstr "تاريخ النشر"

#: src/views/Package.vue:286
msgid "Push Notifications"
msgstr "التنبيهات الفورية"

#: src/views/Package.vue:281
msgid "Read Music Files"
msgstr "قراءة ملفات الموسيقى"

#: src/views/Package.vue:284
msgid "Read Picture Files"
msgstr "قراءة ملفات الصور"

#: src/views/Package.vue:289
msgid "Read Video Files"
msgstr "قراءة ملفات الفيديو"

#: src/views/Browse.vue:41
msgid "Relevance"
msgstr "المشابهة"

#: src/App.vue:103
msgid "Report an Issue"
msgstr "التبليغ عن مشكلة"

#: src/views/Package.vue:356
msgid "Restricted permission"
msgstr "صلاحيات مقيدة"

#: src/views/ManagePackage.vue:294
msgid "Revision"
msgstr "مراجعة"

#: src/App.vue:108 src/views/Feeds.vue:3
msgid "RSS Feeds"
msgstr "تغذيات RSS"

#: src/views/Submit.vue:4
msgid "Rules for Submission"
msgstr "شروط رفع التطبيق"

#: src/views/Submit.vue:31
msgid "Rules for Submissions Requiring Manual Review"
msgstr "شروط رفع التطبيقات التي تحتاج مراجعة يدوية"

#: src/views/About.vue:50
msgid "Run the command:"
msgstr "نفذ الأمر:"

#: src/views/ManagePackage.vue:348
msgid "Save"
msgstr "حفظ"

#: src/components/Types.vue:4 src/views/Stats.vue:109
msgid "Scope"
msgstr "سكوب"

#: src/views/Browse.vue:34
msgid "Scopes"
msgstr "سكوبات"

#: src/views/Package.vue:107
msgid "Screenshots"
msgstr "لقطات شاشة"

#: src/views/ManagePackage.vue:106
msgid "Screenshots (Limit 5)"
msgstr "لقطات شاشة (الحد الأقصى 5)"

#: src/views/Browse.vue:9 src/views/Manage.vue:26
msgid "Search"
msgstr "البحث"

#: src/views/Submit.vue:35
msgid ""
"Send us a link to a repository for your app to our telegram group along with "
"some instructions on how to build it."
msgstr ""

#: src/views/Package.vue:287
msgid "Sensors"
msgstr "مستكشفات"

#: src/views/Submit.vue:74
msgid "Sexually Explicit Content"
msgstr "محتوى جنسي صريح"

#: src/views/Package.vue:117
msgid "Show NSFW Content"
msgstr "عرض محتوى البالغين"

#: src/App.vue:98
msgid "Site Status"
msgstr "إحصائيات الموقع"

#: src/views/Browse.vue:39
msgid "Sort By"
msgstr "فرز وفق"

#: src/views/Package.vue:169
msgid "Source"
msgstr "المصدر"

#: src/views/ManagePackage.vue:206
msgid "Source URL"
msgstr "رابط المصدر"

#: src/views/ManagePackage.vue:65 src/views/Stats.vue:3 src/views/Stats.vue:92
#: src/views/Stats.vue:96
msgid "Stats"
msgstr "إحصائيات"

#: src/views/Manage.vue:48
msgid "Status"
msgstr "الحالة"

#: src/App.vue:28 src/views/Submit.vue:162
msgid "Submit"
msgstr "رفع"

#: src/views/Manage.vue:10 src/views/Submit.vue:129 src/views/Submit.vue:189
#: src/views/Submit.vue:193
msgid "Submit App"
msgstr "رفع تطبيق"

#: src/views/About.vue:134
msgid "Submit your app"
msgstr "ارفع تطبيقك"

#: src/views/ManagePackage.vue:551 src/views/ManagePackage.vue:576
#: src/views/ManageRevisions.vue:177
msgid "Success"
msgstr "نجاح"

#: src/views/Package.vue:165
msgid "Support"
msgstr "الدعم"

#: src/views/ManagePackage.vue:211
msgid "Support URL"
msgstr "رابط الدعم"

#: src/views/ManagePackage.vue:85
msgid "Tag Line"
msgstr "سطر الوسم"

#: src/views/About.vue:102
msgid "Terms"
msgstr "الشروط"

#: src/views/Package.vue:13
msgid "The app you are looking for has been removed or does not exist"
msgstr "التطبيق الذي تبحث عنه قد تم حذفه او غير موجود"

#: src/views/About.vue:97
msgid ""
"The apps available for download from the OpenStore have their own licencing "
"and terms.\n"
"Consult each individual app's page for more information."
msgstr ""
"تملك التطبيقات المتاحة للتنزيل من OpenStore ترخيصها وشروطها الخاصة.\n"
"راجع صفحة كل تطبيق على حدة للحصول على مزيد من المعلومات."

#: src/views/ManagePackage.vue:552
msgid "The changes to %{name} were saved!"
msgstr "لقد تم حفظ التغييرات لـ  %{name}!"

#: src/views/About.vue:104
msgid ""
"The OpenStore a non-profit volunteer project.\n"
"Although every effort is made to ensure that everything in the repository is "
"safe to install, you use it AT YOUR OWN RISK.\n"
"Apps uploaded to the OpenStore will be subject to an automated review "
"process or\n"
"a manual review process to check for potential security or privacy issues.\n"
"This checking is far from exhaustive though, and there are no guarantees."
msgstr ""
"OpenStore مشروع تطوعي غير ربحي.\n"
"على الرغم من جميع الجهود لضمان أن كل شيء في المتجر آمن للتثبيت ، فإنك "
"تستخدمه على مسؤوليتك الخاصة.\n"
"ستخضع التطبيقات التي تم تحميلها إلى OpenStore لعملية مراجعة تلقائية أو\n"
"عملية مراجعة يدوية للتحقق من مشكلات الأمان أو الخصوصية المحتملة.\n"
"هذا التدقيق بعيد عن أن يكون شاملاً ، ولا توجد ضمانات."

#: src/views/About.vue:31
msgid ""
"The OpenStore app is installed by default on the UBports builds of Ubuntu "
"Touch.\n"
"But if you need to install the OpenStore manually, follow these steps:"
msgstr ""
"تطبيق OpenStore مثبت بشكل افتراضي على نسخة UBports من اوبنتو Touch.\n"
"ولكن إذا كنت بحاجة إلى تثبيت OpenStore يدويًا ، فاتبع الخطوات التالية:"

#: src/views/About.vue:141
msgid "The OpenStore is made possible by many amazing contributors:"
msgstr "أصبح مشروع OpenStore ممكنًا بفضل العديد من المساهمين الرائعين:"

#: src/views/About.vue:5
msgid ""
"The OpenStore is the official app store for Ubuntu Touch.\n"
"It is an open source project run by a team of volunteers with help from the "
"community.\n"
"You can discover and install new apps on your Ubuntu Touch device.\n"
"You can also upload and manage your own apps for publication.\n"
"The OpenStore encourages the apps published within to be open source, but "
"also accepts proprietary apps."
msgstr ""
"OpenStore هو متجر التطبيقات الرسمي لـ اوبنتو Touch.\n"
"وهو مشروع مفتوح المصدر يديره فريق من المتطوعين بمساعدة من مجتمع مستخدميه.\n"
"يمكنك اكتشاف وتصفح التطبيقات الجديدة وتثبيتها على جهاز اوبنتو Touch.\n"
"يمكنك أيضًا تحميل وإدارة تطبيقاتك الخاصة للنشر.\n"
"يشجع OpenStore التطبيقات المنشورة في أن تكون مفتوحة المصدر ، ولكنه يقبل أيضًا "
"تطبيقات الملكية."

#: src/views/About.vue:112
msgid ""
"The OpenStore respects your privacy.\n"
"We don't track you or your device.\n"
"You do not need an account to download and install apps and we do not track "
"your downloads.\n"
"For developer information we count the number of downloads per app "
"revision.\n"
"Information about your device (the cpu architecture, framework versions, OS "
"version, and OS language)\n"
"are sent to the server to allow it to filter apps to only what you can "
"install and present your native language where possible.\n"
"This information is not logged or stored and cannot be used to uniquely "
"identify you."
msgstr ""
"يحترم OpenStore خصوصيتك.\n"
"نحن لا نتتبعك أو جهازك.\n"
"لست بحاجة إلى حساب لتحميل التطبيقات وتثبيتها ونحن لا نتتبع عمليات التحميل "
"الخاصة بك.\n"
"بالنسبة إلى معلومات مطوري البرامج ، نحسب عدد التحميلات لكل مراجعة تطبيق.\n"
"معلومات حول جهازك (بنية CPU ، وإصدارات منصة العمل ، وإصدار نظام التشغيل ، "
"ولغة نظام التشغيل)\n"
"يتم إرسالها إلى الخادم للسماح بتصفية التطبيقات إلى ما يمكنك تثبيته وعرض لغتك "
"الأصلية قدر الإمكان.\n"
"لا يتم تسجيل أو تخزين هذه المعلومات ولا يمكن استخدامها لتحديد هويتك بشكل خاص."

#: src/views/NotFound.vue:5
msgid "The page you are looking for has been removed or does not exist"
msgstr "الصفحة التي تبحث عنها قد تم إزالتها او غير موجودة"

#: src/views/About.vue:80
msgid ""
"The source code for OpenStore related projects is available under the GPL "
"v3.\n"
"You can find the code on the OpenStore GitLab page."
msgstr ""
"الكود المصدري للمشاريع المرتبطة بـ OpenStore متوفر تحت ترخيص GPL v3.\n"
"يمكنك العثور على الكود المصدري في صفحة OpenStore GitLab."

#: src/views/Package.vue:17
msgid ""
"There was an error trying to find this app, please refresh and try again."
msgstr ""
"لقد حدث خطأ اثناء المحاولة للعثور على هذا التطبيق, يرجى تحديث الصفحة وإعادة "
"المحاولة."

#: src/views/Browse.vue:65 src/views/Manage.vue:31
msgid ""
"There was an error trying to load the app list, please refresh and try again."
msgstr ""
"لقد حدث خطأ اثناء المحاولة لعرض قائمة التطبيقات, يرجى تحديث الصفحة وإعادة "
"المحاولة."

#: src/views/Stats.vue:5
msgid ""
"There was an error trying to load the stats, please refresh and try again."
msgstr ""
"لقد حدث خطأ اثناء المحاولة لعرض الإحصائيات, يرجى تحديث الصفحة وإعادة "
"المحاولة."

#: src/views/ManagePackage.vue:163
msgid "This app contains NSFW material"
msgstr "هذا التطبيق يتضمن محتوى للبالغين فقط"

#: src/views/Package.vue:368
msgid ""
"This app does not have access to restricted system data, see permissions for "
"more details"
msgstr ""
"لا يمتلك هذا التطبيق إمكانية الوصول إلى بيانات النظام المقيدة، راجع "
"الصلاحيات لمزيد من التفاصيل"

#: src/views/Package.vue:370
msgid ""
"This app has access to restricted system data, see permissions for more "
"details"
msgstr ""
"هذا التطبيق لديه صلاحية الوصول إلى بيانات النظام المقيدة ، راجع الصلاحيات "
"لمزيد من التفاصيل"

#: src/views/Submit.vue:141
msgid ""
"This is the unique identifier for your app.\n"
"It must match exactly the \"name\" field in your click's manifest.json and "
"must be all lowercase letters.\n"
"For example: \"openstore.openstore-team\", where \"openstore\" is the app "
"and \"openstore-team\"\n"
"is the group or individual authoring the app."
msgstr ""
"هذا هو المعرف الفريد لتطبيقك.\n"
"يجب أن يتطابق تمامًا مع حقل \"الاسم\" في ملف  manifest.json الخاص بتطبيقك "
"ويجب أن يكون جميع الأحرف الصغيرة.\n"
"على سبيل المثال: \"openstore.openstore-team\" ، حيث \"openstore\" هو التطبيق "
"و \"openstore-team\"\n"
"هي المجموعة أو الشخص الذي يقوم برفع التطبيق."

#: src/views/ManageRevisions.vue:52
msgid "This will be added to the beginning of your current changelog"
msgstr ""

#: src/views/ManagePackage.vue:80
msgid "Title"
msgstr "العنوان"

#: src/views/Browse.vue:42
msgid "Title (A-Z)"
msgstr "العنوان (A-Z)"

#: src/views/Browse.vue:43
msgid "Title (Z-A)"
msgstr "العنوان (Z-A)"

#: src/views/ManagePackage.vue:314
msgid "Total"
msgstr "المجموع"

#: src/views/Manage.vue:79
#, fuzzy
msgid "Total:"
msgstr "المجموع"

#: src/views/ManagePackage.vue:279 src/views/Package.vue:197
msgid "Translation Languages"
msgstr "لغات الترجمة"

#: src/views/Browse.vue:102 src/views/Manage.vue:95
msgid "Try searching for something else"
msgstr "حاول البحث عن شيء آخر"

#: src/views/Browse.vue:29 src/views/ManagePackage.vue:270
msgid "Type"
msgstr "النوع"

#: src/views/About.vue:14
msgid "Ubuntu Touch"
msgstr "اوبنتو Touch"

#: src/views/Browse.vue:3
msgid "Ubuntu Touch Apps"
msgstr "تطبيقات اوبنتو Touch"

#: src/views/Stats.vue:106
msgid "Uncategorized"
msgstr "غير مصنف"

#: src/views/Package.vue:181
msgid "Updated"
msgstr "تم تحديثه"

#: src/views/Feeds.vue:18
msgid "Updated Apps"
msgstr "التطبيقات المُحدثة"

#: src/views/ManagePackage.vue:331
msgid "Updated Date"
msgstr "تاريخ التحديث"

#: src/views/ManageRevisions.vue:36
msgid "URL"
msgstr "رابط"

#: src/views/ManageRevisions.vue:44
msgid "URL of App from the Web"
msgstr ""

#: src/views/Package.vue:288
msgid "User Metrics"
msgstr "مقاييس المستخدم"

#: src/views/Manage.vue:49 src/views/ManagePackage.vue:296
msgid "Version"
msgstr "رقم النسخة"

#: src/views/Package.vue:291
msgid "Video"
msgstr "فيديو"

#: src/views/Package.vue:290
msgid "Video Files"
msgstr "ملفات الفيديو"

#: src/views/ManagePackage.vue:221
msgid "Video URL"
msgstr "رابط الفيديو"

#: src/views/About.vue:37
msgid "View Install Instructions"
msgstr "عرض تعليمات التثبيت"

#: src/views/Submit.vue:87
msgid "Violence"
msgstr "عنف"

#: src/components/Types.vue:5 src/views/Stats.vue:110
msgid "Web App"
msgstr "تطبيق ويب"

#: src/components/Types.vue:6 src/views/Stats.vue:111
msgid "Web App+"
msgstr "تطبيق ويب+"

#: src/views/Browse.vue:33
msgid "Web Apps"
msgstr "تطبيقات الويب"

#: src/views/Package.vue:292
msgid "Webview"
msgstr "Webview"

#: src/views/Manage.vue:4
msgid "Welcome %{name}!"
msgstr ""

#: src/views/ManageRevisions.vue:15 src/views/ManageRevisions.vue:174
msgid "Xenial"
msgstr "Xenial"

#: src/views/ManagePackage.vue:30
msgid "Yes"
msgstr "نعم"

#: src/views/Submit.vue:10
msgid ""
"You are only allowed to publish apps that you have permission to distribute."
msgstr "لا يُسمح لك سوى بنشر التطبيقات التي لديك صلاحية لتوزيعها."

#: src/views/Submit.vue:22
msgid "You must read over the content policy detailed below."
msgstr "يجب عليك قراءة سياسة المحتوى المفصلة أدناه."

#: src/views/Submit.vue:13
msgid ""
"Your app can be pulled without warning at the discretion of our admins.\n"
"Where possible, we will contact you regarding any such actions."
msgstr ""
"يمكنك سحب تطبيقك بدون تحذير وفقًا لتقدير مشرفي المتجر.\n"
"وعند الإمكانية، سنتصل بك بخصوص أي من هذه الإجراءات."

#~ msgid "All Channels"
#~ msgstr "جميع القنوات"

#~ msgid "Download Vivid"
#~ msgstr "حمّل Vivid"

#~ msgid "Download Xenial"
#~ msgstr "حمّل Xenial"

#~ msgid "Filters"
#~ msgstr "الفلاتر"

#~ msgid ""
#~ "Only choose \"Vivid and Xenial\" if your app is qml only or a webapp."
#~ msgstr ""
#~ "قم بإختيار \"Vivid and Xenial\" في حال كان تطبيقك مبني على QML فقط او "
#~ "تطبيق webapp."

#~ msgid "Vivid"
#~ msgstr "Vivid"

#~ msgid "Vivid & Xenial"
#~ msgstr "Vivid و Xenial"

#~ msgid "Vivid and Xenial"
#~ msgstr "Vivid و Xenial"
